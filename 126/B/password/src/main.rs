//------------------------------------- begin input macro ------------------------------------
macro_rules! input {
    (source = $s:expr, $($r:tt)*) => { //if we match source = then an expression, then a coma, and then a token tree we go here
        let mut iter = $s.split_whitespace();
        input_inner!{iter, $($r)*} //send the parsed expression and the token tree after the coma to input_inner
    };
    ($($r:tt)*) => { // if there is no source, and just token tree passed by the AST, we go there
        let s = {
            use std::io::Read;
            let mut s = String::new();
            std::io::stdin().read_to_string(&mut s).unwrap(); //no source given, so we take from stdin
            s
        };
        let mut iter = s.split_whitespace(); //we create an iterator that we split on whitespace
        input_inner!{iter, $($r)*} //internal macro where we pass iter (thing that we got in stdin) and the structure we passed in the macro
    };
    () => {};
}

macro_rules! input_inner {
    //($iter:expr) => {};
    ($iter:expr, ) => {};
    ($iter:expr, $var:ident : $t:tt $($r:tt)*) => { //can't understand
        let $var = read_value!($iter, $t); //will pass to read_value
        input_inner!{$iter $($r)*}
    };
    () => {};
}

macro_rules! read_value {
    ($iter:expr, ( $($t:tt),* )) => {
        ( $(read_value!($iter, $t)),* ) //can't understand
    };
    ($iter:expr, [ $t:tt ; $len:expr ]) => {
        (0..$len).map(|_| read_value!($iter, $t)).collect::<Vec<_>>() //if we match values given on the same line, and a vector written like array a token tree then a ; and then an expression which will be the length of the array we go there and map the values in the array
    };
    ($iter:expr, chars) => {
        read_value!($iter, String).chars().collect::<Vec<char>>() //if of type chars (iterator over the characters of a string) collect them and return et Vec of char
    };
    ($iter:expr, bytes) => {
        read_value!($iter, String).bytes().collect::<Vec<u8>>() //if type bytes, return a vec of u8
    };
    ($iter:expr, usize1) => {
        read_value!($iter, usize) - 1 //if usize1 (for topcoder that gives index starting at 1) will read usize and sub 1
    };
    ($iter:expr, $t:ty) => {
       $iter.next().unwrap().parse::<$t>().expect("Parse error") // after reading a value, we usually match a type, so we got to the next value to read 
    };
    () => {panic!()};
}
// ---------------------------------- end input macro -----------------------------------------
    use std::cmp;
    use std::io;    
    use std::process;
    fn z_function(s: &str) -> Vec<usize> {
        let n = s.len();
        let s_: Vec<_>  = s.chars().collect();
        let mut z = vec![0; n];
        let mut l = 0;
        let mut r = 1;
        for i in 1..n {
            if i <= r {
                z[i] = cmp::min(z[i - l], r - i + 1);
            }
            while i + z[i] < n && s_[i + z[i]] == s_[z[i]] {
                z[i] += 1;
                // print!(".");
            }
            if i + z[i] >= r {
                l = i;
                r = z[i] + i - 1;
            }
            // println!("l = {}, r = {}", l, r);
        }
        return z;
    }
     
     
     
     
     
    fn main() {
        let mut s_ = String::new();
        io::stdin().read_line(&mut s_);
        let s = &s_.trim();
        let chars : Vec<_>  = s.chars().collect();
        let n = s.len();
        if(n<=2){
            println!("Just a legend");
            process::exit(0x0);
        }
        let mut z = z_function(s);
        let mut found = false;
        let mut max_z: usize = 0;
        for (i, &zi) in z.iter().enumerate() {
     //       println!("i is: {}, zi is: {}",i,&zi);
            if max_z >= zi  && i + zi == n {
                let fix: String = chars[0..zi].iter().collect();
                print!("{}", fix);
                found = true;
                break;
            }
            max_z = cmp::max(zi, max_z);
        }
        if !found {
            print!("Just a legend")
        }
    }
