use std::collections::VecDeque;
use std::collections::btree_set::BTreeSet;
use std::cmp::Ordering;

pub fn solve(vect : Vec<u64>, to_reach : Vec<u64>, sums: Vec<u64>,mut v : VecDeque<(usize,usize)>) {
    let mut buf_sum : BTreeSet<u64> =  BTreeSet::new();
    v.push_back((0,vect.len()-1));
    while !v.is_empty(){
        let (left, right) = v.pop_front().unwrap();
 //       println!("left is: {}, right is: {}", left,right);
        buf_sum.insert(sums[right] - ( if left == 0 {0} else {sums[left-1]})); 
        if(vect[left] == vect[right]){ continue; }
        let mid : u64 = vect[left] + ((vect[right] - vect[left])/2);
        let idx = vect.binary_search_by(
                |x| if *x <= mid {Ordering::Less} else {Ordering::Greater}
            ).unwrap_err();
            v.push_back((left, idx-1));
            v.push_back((idx, right));
    }
    for i in 0.. to_reach.len(){
        if buf_sum.contains(&to_reach[i]){
   //         println!("the different sums were {:?}, the searched value is {}", buf_sum, to_reach[i]);
            println!("Yes");
        }
        else{
     //       println!("the different sums were {:?}, the searched value is {}", buf_sum, to_reach[i]);
            println!("No");
        }
    }
}


fn main() {

    let (r, w) = (std::io::stdin(), std::io::stdout());
    let mut sc = IO::new(r.lock(), w.lock());
    let ntc = sc.read();
    for _ in 0..ntc{
        let (n,num) = (sc.read(), sc.read());
        let mut  vect = vec![0;n];
        let mut to_reach = vec![0;num];
        
        for i in 0..n{
            vect[i] = sc.read::<u64>();
        }
        for i  in 0..num{
            to_reach[i] = sc.read::<u64>();
        }
        //the sum can be calculated using prefix sum apparently https://www.dcc.fc.up.pt/~pribeiro/aulas/pc2021/material/prefixsums.html
        vect.sort();
       // println!("vect is {:?}, and to_reach is {:?}", vect, to_reach);
        let mut sums = vec![0;n];
        sums[0] = vect[0];
        for i in 1..n{
            sums[i] = sums[i-1] + vect[i];
        }
       // println!("the sum is {:?}",sums);
        let mut v : VecDeque<(usize,usize)> = VecDeque::new();
        solve(vect, to_reach, sums, v);
    }
 //   println!("out");
    } 


//taken from coco18000 there https://codeforces.com/contest/1462/submission/110554968

pub struct IO<R, W: std::io::Write>(R, std::io::BufWriter<W>);

impl<R: std::io::Read, W: std::io::Write> IO<R, W> {
    pub fn new(r: R, w: W) -> IO<R, W> {
        IO(r, std::io::BufWriter::new(w))
    }
    pub fn write<S: ToString>(&mut self, s: S) {
        use std::io::Write;
        self.1.write_all(s.to_string().as_bytes()).unwrap();
    }
    pub fn read<T: std::str::FromStr>(&mut self) -> T {
        use std::io::Read;
        let buf = self
            .0
            .by_ref()
            .bytes()
            .map(|b| b.unwrap())
            .skip_while(|&b| b == b' ' || b == b'\n' || b == b'\r' || b == b'\t')
            .take_while(|&b| b != b' ' && b != b'\n' && b != b'\r' && b != b'\t')
            .collect::<Vec<_>>();
        unsafe { std::str::from_utf8_unchecked(&buf) }
            .parse()
            .ok()
            .expect("Parse error.")
    }
    pub fn usize0(&mut self) -> usize {
        self.read::<usize>() - 1
    }
    pub fn vec<T: std::str::FromStr>(&mut self, n: usize) -> Vec<T> {
        (0..n).map(|_| self.read()).collect()
    }
    pub fn chars(&mut self) -> Vec<char> {
        self.read::<String>().chars().collect()
    }
}
