use std::cmp; 
#[allow(unused_parens)]
pub fn z_function(s : Vec<u8>, n : usize) -> Vec<usize> {
    // retourner uniquement les valeurs non nulles dans le tableau
    let mut l = 0;
    let mut r = 0;
    let mut z = vec![0;n];
  //  let mut found = HashMap::new();
    for i in 1..n{
        if (i<=r){
            z[i] = cmp::min(r-i+1,z[i-l]);
        }
        else{
           z[i]   = 0;
        }
        while(i +z[i]   < n && s[z[i]] == s[i + z[i]] ){
            z[i] = z[i]  +1;
        }
        if(i +z[i]   -1 > r){
            l = i ;
            r = i +z[i]-1;
        }
    }

   // return found;
   //return solv;
   z
}
#[allow(unused_parens)]
fn main() {
    let mut str = String::new();
	std::io::stdin().read_line(&mut str).expect("");
    let s = str.trim().as_bytes().to_vec();   
    //let s : Vec<u8> = "ABACABA".to_string().bytes().collect();
    //let s : Vec<u8> = "AAAA".to_string().bytes().collect();
    //let s : Vec<u8> = "AAA".to_string().bytes().collect();
    let n = s.len();
    let mut z1 = z_function(s,n);
    let mut results = vec![0;n+1];
    let mut solv = Vec::new();
    for i in  0..z1.len(){
        results[z1[i]] += 1;
    }
 //   println!("z is {:?}",z1);
    for i in (1..n).rev(){
        results[i] += results[i+1];
    }
    for i in (0..z1.len()).rev(){
        //TODO
        // Si idx + z[i] == len --> c'est un prefixe on est bon donc on le compte (on push)
        // keeps buffer which stands for every match, if there are greater number that match e.g
        // z[28] == 3, if z[9] == 2, the total count for z[9] will have to take in account, since
        // we check only indexes
        if(z1[i] + i == n){
            solv.push((z1[i],results[z1[i]]));
        }

    }
    println!("{}",solv.len()+1);   
    for (a,b) in solv{
        println!("{} {}",a,b+1);
    }
    println!("{} 1",n);
}
